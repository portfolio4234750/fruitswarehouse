﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;

using FruitsWarehouse.ViewModels;

namespace FruitsWarehouse.Views;

public partial class AddSupplyWindow : ReactiveWindow<AddSupplyViewModel> {
	public AddSupplyWindow() {
		InitializeComponent();
	}
}