﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using FruitsWarehouse.Models.DB;
using FruitsWarehouse.ViewModels;
using Microsoft.Extensions.Options;

namespace FruitsWarehouse.Views;

public partial class CreateReportWindow : ReactiveWindow<CreateReportViewModel> {
	public CreateReportWindow() {
		InitializeComponent();
	}
}