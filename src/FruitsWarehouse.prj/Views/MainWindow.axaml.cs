using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.ReactiveUI;
using Avalonia.Threading;
using FruitsWarehouse.Models.DB;
using FruitsWarehouse.ViewModels;
using Microsoft.Extensions.Options;
using ReactiveUI;

namespace FruitsWarehouse.Views;

public partial class MainWindow : ReactiveWindow<MainWindowViewModel> {
	public MainWindow() {
		InitializeComponent();

		this.WhenActivated( _ => {
			ViewModel.AddSupplyInteraction.RegisterHandler(ShowAddSupplyWindowAsync);
			ViewModel.CreateReportInteraction.RegisterHandler(ShowCreateReportWindowAsync);
		});
	}

	private async Task ShowAddSupplyWindowAsync(InteractionContext<IOptions<DBCredentials>,
		bool> interactionContext) {
		var vm = new AddSupplyViewModel(interactionContext.Input);
		var window = new AddSupplyWindow {
			DataContext = vm
		};

		await window.ShowDialog(this);

		interactionContext.SetOutput(vm.IsChanged);
	}

	private async Task ShowCreateReportWindowAsync(InteractionContext<IOptions<DBCredentials>,
		Unit> interactionContext) {
		var vm = new CreateReportViewModel(interactionContext.Input);
		var window = new CreateReportWindow {
			DataContext = vm
		};

		await window.ShowDialog(this);

		interactionContext.SetOutput(Unit.Default);
	}
}