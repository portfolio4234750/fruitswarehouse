﻿using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows.Input;
using Avalonia.Collections;
using ReactiveUI;
using FruitsWarehouse.Models.DB;
using Microsoft.Extensions.Options;

namespace FruitsWarehouse.ViewModels;

public class MainWindowViewModel : ViewModelBase {
	#region Properties

	public AvaloniaList<Remain> Remains { get; }

	#endregion

	#region .ctor

	public MainWindowViewModel(IOptions<DBCredentials> creds) {
		using (var database = new DBApplicationContext(creds))
			Remains = new AvaloniaList<Remain>(database.Remains.ToArray());

		AddSupplyCommand = ReactiveCommand.CreateFromTask(async () => {
			if (await AddSupplyInteraction?.Handle(creds)) {
				Remains.Clear();
				await using (var database = new DBApplicationContext(creds))
					Remains.AddRange(database.Remains.ToArray());
			}
		});

		CreateReportCommand = ReactiveCommand.CreateFromTask(async () => {
			await CreateReportInteraction?.Handle(creds);
		});
	}

	#endregion

	#region Commands

	public ICommand AddSupplyCommand { get; }
	public ICommand CreateReportCommand { get; }

	#endregion

	#region Interactions

	public Interaction<IOptions<DBCredentials>, bool> AddSupplyInteraction = new ();
	public Interaction<IOptions<DBCredentials>, Unit> CreateReportInteraction = new ();

	#endregion
}