﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows.Input;
using Avalonia.Collections;
using Avalonia.Controls;
using FruitsWarehouse.Models;
using FruitsWarehouse.Models.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReactiveUI;

namespace FruitsWarehouse.ViewModels;

public class AddSupplyViewModel : ViewModelBase {
	#region Fields

	private AddSupplyModel _model;

	#endregion

	#region Properties

	public AvaloniaList<Vendor> Vendors { get; }
	public int SelectedVendor { get; set; }
	public AvaloniaList<Supply> Products { get; }
	public bool IsChanged { get; private set; }

	#endregion

	#region .ctor

	public AddSupplyViewModel(IOptions<DBCredentials> creds) {
		_model = new AddSupplyModel();
		IsChanged = false;
		using (var database = new DBApplicationContext(creds))
			Vendors = new AvaloniaList<Vendor>(database.Vendors.ToArray());
		SelectedVendor = 0;
		Products = new AvaloniaList<Supply>();

		AddProductCommand = ReactiveCommand.CreateFromTask(async () => {
			await using (var database = new DBApplicationContext(creds))
				Products.Add(new Supply() {
					Id = int.Max(await database.Supplies.AnyAsync()
						? await database.Supplies.MaxAsync(x => x.Id) + 1
						: 1,
						Products.Any()
						? Products.MaxBy(x => x.Id).Id + 1
						: 1),
					Date = DateTime.Today,
					VendorId = Vendors[SelectedVendor].Id,
					VendorName = Vendors[SelectedVendor].Name
				});
		});

		AddSupplyCommand = ReactiveCommand.CreateFromTask<Window>(async (window) => {
			if (Products.Any(x => x.ProductType == null ||  x.Weight == null || x.Price == null)) return;
			foreach (var t in Products)
				t.ProductType = t.ProductType switch {
					"0" => "Яблоки Красные",
					"1" => "Яблоки Зеленые",
					"2" => "Груши Деведжи",
					"3" => "Груши Вильямс",
					_ => t.ProductType
				};
			await _model.AddSupply(creds, Products);
			IsChanged = true;
			window.Close();
		});

		CloseCommand = ReactiveCommand.Create<Window>((window) => {
			window.Close();
		});
	}

	#endregion

	#region Commands

	public ICommand AddProductCommand { get; }
	public ICommand AddSupplyCommand { get; }
	public ICommand CloseCommand { get; }

	#endregion
}