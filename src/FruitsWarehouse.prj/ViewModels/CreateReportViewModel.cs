﻿using System;
using System.Linq;
using System.Windows.Input;

using Avalonia.Collections;
using ReactiveUI;

using FruitsWarehouse.Models.DB;

using Microsoft.Extensions.Options;

namespace FruitsWarehouse.ViewModels;

public class CreateReportViewModel {
	#region Properties

	public AvaloniaList<Supply> Supplies { get; }

	public DateTime? DateStart { get; set; }

	public DateTime? DateEnd { get; set; }

	public IOptions<DBCredentials> Credentials { get; }

	#endregion

	#region .ctor

	public CreateReportViewModel(IOptions<DBCredentials> creds) {
		Credentials = creds;
		Supplies = new AvaloniaList<Supply>();

		ShowSuppliesCommand = ReactiveCommand.Create(() => {
			if (DateStart == null || DateEnd == null) return;
			Supplies.Clear();
			using (var database = new DBApplicationContext(creds)) {
				Supplies.AddRange(database.Supplies.Where(x => x.Date >= DateStart.Value && x.Date <= DateEnd.Value));
			}
		});
	}

	#endregion

	#region Commands

	public ICommand ShowSuppliesCommand { get; }

	#endregion
}