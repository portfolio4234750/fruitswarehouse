﻿using System;
using System.Globalization;

using Avalonia.Data;
using Avalonia.Data.Converters;

using FruitsWarehouse.Models.DB;

namespace FruitsWarehouse.Converters;

public class CalculateCostConverter : IValueConverter {
	public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
	{
		var entity = (Supply)value;
		return (entity.Price * entity.Weight).ToString();
	}

	public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
	{
		return BindingOperations.DoNothing;
	}
}