﻿using System;
using System.Globalization;

using Avalonia.Data;
using Avalonia.Data.Converters;

namespace FruitsWarehouse.Converters;

public class DateTimeToDateOnlyConverter : IValueConverter {
	public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
	{
		var entity = (DateTime)value;
		return DateOnly.FromDateTime(entity).ToString();
	}

	public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
	{
		return BindingOperations.DoNothing;
	}
}