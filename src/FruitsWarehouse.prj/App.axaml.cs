using System;
using System.IO;
using Serilog;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using FruitsWarehouse.Models;
using ReactiveUI;
using FruitsWarehouse.Models.DB;
using FruitsWarehouse.ViewModels;
using FruitsWarehouse.Views;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Splat;
using Splat.Microsoft.Extensions.DependencyInjection;

namespace FruitsWarehouse;

public partial class App : Application {
	public IServiceProvider Container { get; private set; }

	/// <summary>Возвращает конфигурацию приложения.</summary>
	/// <value>Конфигурация приложения.</value>
	public static IConfiguration Configuration { get; set; }

	public override void Initialize() {
		AvaloniaXamlLoader.Load(this);
	}

	public App() {
		var host = Host
			.CreateDefaultBuilder()
			.UseSerilog()
			.ConfigureAppConfiguration((config) => {
				config.Sources.Clear();

				if (!File.Exists(PathsConstants.ConfigFilePath))
					throw new FileNotFoundException(
						$"Configuration file: {PathsConstants.ConfigFilePath} couldn't be found");

				Configuration = config
					.SetBasePath(PathsConstants.ConfigDirPath)
					.AddJsonFile(PathsConstants.ConfigFileName, false, false)
					.Build();
			})
			.ConfigureServices(service => {
				service.UseMicrosoftDependencyResolver();

				service.Configure<DBCredentials>(Configuration.GetSection("db"));
				service
					.AddScoped<DBApplicationContext>()
					.AddSingleton<AddSupplyModel>()
					.AddSingleton<AddSupplyViewModel>()
					.AddSingleton<CreateReportViewModel>()
					.AddSingleton<MainWindowViewModel>();

				var resolver = Locator.CurrentMutable;
				resolver.InitializeSplat();
				resolver.InitializeReactiveUI();

				RxApp.MainThreadScheduler = AvaloniaScheduler.Instance;
			}).Build();

		Container = host.Services;
		Container.UseMicrosoftDependencyResolver();

		host.Start();
	}

	public override void OnFrameworkInitializationCompleted() {
		if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop) {
			var mainViewModel = Container.GetRequiredService<MainWindowViewModel>();
			desktop.MainWindow = new MainWindow {
				DataContext = mainViewModel
			};
		}

		base.OnFrameworkInitializationCompleted();
	}
}