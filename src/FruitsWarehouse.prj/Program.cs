﻿using Avalonia;
using Avalonia.ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace FruitsWarehouse;

internal class Program {
	/// <summary>Возвращает уровень логгирования.</summary>
	/// <value>Уровень логгирования.</value>
	public static LoggingLevelSwitch LoggingLevel { get; private set; }

	// Initialization code. Don't use any Avalonia, third-party APIs or any
	// SynchronizationContext-reliant code before AppMain is called: things aren't initialized
	// yet and stuff might break.
	[STAThread]
	public static void Main(string[] args) {
		CreateNonExistingDirectories(new List<string>() {
			PathsConstants.AppProgramDataPath,
			PathsConstants.LogsRootPath
		});

		LoggingLevel = new LoggingLevelSwitch(LogEventLevel.Debug);
		Log.Logger = SwitchableLogger.Instance.Logger = SwitchableLogger.Instance;
		SwitchableLogger.Instance.Logger = GetBaseLoggerConfig().CreateLogger();

		AppDomain.CurrentDomain.UnhandledException += ExceptionHandler;

		BuildAvaloniaApp()
			.StartWithClassicDesktopLifetime(args);
	}

	private static void CreateNonExistingDirectories(IEnumerable<string> paths) {
		// foreach(var path in paths)
		// {
		// 	if(!Directory.Exists(path)) Directory.CreateDirectory(path);
		// }
	}

	private static LoggerConfiguration GetBaseLoggerConfig() {
		const int logFileSizeLimit = 10 * 1024 * 1024;

		var path = PathsConstants.LogsRootPath;

		return new LoggerConfiguration()
			.MinimumLevel.ControlledBy(LoggingLevel)
			.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
			.MinimumLevel.Override("System", LogEventLevel.Warning)
			.Enrich.FromLogContext()
			.WriteTo.Logger(lc =>
				lc.Filter.ByIncludingOnly(logEvent => logEvent.Level <= LogEventLevel.Debug)
					.WriteTo.File
					(
						Path.Combine(path, "Logs.dbg_.txt"),
						rollOnFileSizeLimit: true,
						rollingInterval: RollingInterval.Day,
						fileSizeLimitBytes: logFileSizeLimit
					))
			.WriteTo.Logger(lc =>
				lc.Filter.ByIncludingOnly(logEvent => logEvent.Level == LogEventLevel.Verbose)
					.WriteTo.File
					(
						Path.Combine(path, "Logs.trc_.txt"),
						rollOnFileSizeLimit: true,
						rollingInterval: RollingInterval.Day,
						fileSizeLimitBytes: logFileSizeLimit
					))
			.WriteTo.File
			(
				Path.Combine(path, "Logs.err_.txt"),
				rollOnFileSizeLimit: true,
				rollingInterval: RollingInterval.Day,
				fileSizeLimitBytes: logFileSizeLimit,
				restrictedToMinimumLevel: LogEventLevel.Error
			)
			.WriteTo.File
			(
				Path.Combine(path, "Logs_.txt"),
				rollOnFileSizeLimit: true,
				rollingInterval: RollingInterval.Day,
				fileSizeLimitBytes: logFileSizeLimit,
				restrictedToMinimumLevel: LogEventLevel.Debug
			);
	}

	private static void ExceptionHandler(object sender, UnhandledExceptionEventArgs args) {
		var Log = Serilog.Log.ForContext<Program>();
		var e = (Exception)args.ExceptionObject;
		Log.Fatal(e, "Unhandled error: {Exception}", e.Message);
	}

	// Avalonia configuration, don't remove; also used by visual designer.
	public static AppBuilder BuildAvaloniaApp()
		=> AppBuilder.Configure<App>()
			.UsePlatformDetect()
			.WithInterFont()
			.LogToTrace()
			.UseReactiveUI();
}