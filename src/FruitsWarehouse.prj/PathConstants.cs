﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.IO;

namespace FruitsWarehouse;

public static class PathsConstants {
	#region Data

	/// <summary>Сборка, которая начала исполнение приложения.</summary>
	private static readonly Assembly CallingAssembly = Assembly.GetEntryAssembly()!;

	/// <summary>Информация о файла сборки, которая начала исполнение приложения.</summary>
	private static readonly FileVersionInfo VersionInfo = FileVersionInfo.GetVersionInfo(
		CallingAssembly.Location);

	/// <summary>Путь к общей для всех пользователей в системе папке, где хранятся данные приложения.</summary>
	private static readonly string ProgramDataPath = Environment.GetFolderPath(
		Environment.SpecialFolder.CommonApplicationData);

	#endregion

	#region Properties

	/// <summary>Возвращает путь к общей для всех пользователей в системе папке, где хранятся данные приложения.</summary>
	/// <value>Путь, по которому будут сохраняться данные приложения.</value>
	public static string AppProgramDataPath => Path.Combine(
		ProgramDataPath,
		Path.GetFileNameWithoutExtension(VersionInfo?.InternalName));

	/// <summary>Возвращает путь, по которому будут сохраняться протоколы работы приложения.</summary>
	/// <value>Путь, по которому будут сохраняться протоколы работы приложения.</value>
	public static string LogsRootPath => Path.Combine(
		AppProgramDataPath,
		"Logs");

	/// <summary>Возвращает путь до папки, где хранится файл конфигурации.</summary>
	/// <value>Путь до папки, по которому хранится конфигурация .</value>
	public static string ConfigDirPath => Path.Combine(Environment.CurrentDirectory, "Config");

	/// <summary>Возвращает имя файла, в котором хранится конфигурации.</summary>
	/// <value>Имя файла конфигурации .</value>
	public static string ConfigFileName => "configuration.json";

	/// <summary>Возвращает путь, где хранится файл конфигурации.</summary>
	/// <value>Путь, по которому хранится конфигурация .</value>
	public static string ConfigFilePath => Path.Combine(ConfigDirPath, ConfigFileName);

	#endregion
}