﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using FruitsWarehouse.Models.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace FruitsWarehouse.Models;

public class AddSupplyModel {
	public async Task AddSupply(IOptions<DBCredentials> creds, IEnumerable<Supply> products) {
		await using (var database = new DBApplicationContext(creds)) {
			var remainId = await database.Remains.AnyAsync()
				? await database.Remains.MaxAsync(x => x.Id) + 1
				: 1;
			foreach (var product in products) {
				await database.Supplies.AddAsync(product);
				var remain = database.Remains
					.Where(x => x.ProductType == product.ProductType);
				if (remain.Any()) {
					(await remain.FirstAsync()).TotalWeight += product.Weight.Value;
				} else {
					await database.Remains.AddAsync(new Remain() {
						Id = remainId++,
						ProductType = product.ProductType,
						TotalWeight = product.Weight.Value
					});
				}
			}
			await database.SaveChangesAsync();
		}
	}
}