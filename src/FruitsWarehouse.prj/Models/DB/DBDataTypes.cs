﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace FruitsWarehouse.Models.DB;

public record Vendor {
	public int Id { get; set; }
	public string Name { get; set; }
}

public record Supply {
	public int Id { get; set; }
	public DateTime Date { get; set; }
	public int VendorId { get; set; }
	public string VendorName { get; set; }
	public string? ProductType { get; set; }
	public float? Weight { get; set; }
	public float? Price { get; set; }
}

public record Remain {
	public int Id { get; set; }
	public string ProductType { get; set; }
	public float TotalWeight { get; set; }
}

public record DBCredentials {
	public string Server { get; set; }
	public string Database { get; set; }
	public string User { get; set; }
	public string Password { get; set; }
}