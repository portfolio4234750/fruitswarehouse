﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MySql.EntityFrameworkCore.Extensions;

namespace FruitsWarehouse.Models.DB;

public sealed class DBApplicationContext : DbContext {
	#region Data

	public DbSet<Vendor> Vendors { get; set; }
	public DbSet<Supply> Supplies { get; set; }
	public DbSet<Remain> Remains { get; set; }

	private readonly IOptions<DBCredentials> Credentials;

	#endregion

	#region .ctor

	public DBApplicationContext(IOptions<DBCredentials> credentials) {
		Credentials = credentials;
		Database.EnsureCreated();
	}

	#endregion

	#region Methods

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
		optionsBuilder.UseMySQL(
			$"server={Credentials.Value.Server};database={Credentials.Value.Database};user={Credentials.Value.User};password={Credentials.Value.Password}");
		optionsBuilder.EnableSensitiveDataLogging();
	}

	#endregion
}