CREATE DATABASE  IF NOT EXISTS `warehouse` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `warehouse`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: warehouse
-- ------------------------------------------------------
-- Server version	8.0.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `supplies`
--

DROP TABLE IF EXISTS `supplies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supplies` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `vendorId` int unsigned NOT NULL,
  `vendorName` varchar(50) NOT NULL,
  `productType` varchar(50) NOT NULL,
  `weight` decimal(10,2) unsigned NOT NULL,
  `price` decimal(5,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendorId_idx` (`vendorId`),
  KEY `vendorName_idx` (`vendorName`),
  CONSTRAINT `vendorId` FOREIGN KEY (`vendorId`) REFERENCES `vendors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplies`
--

LOCK TABLES `supplies` WRITE;
/*!40000 ALTER TABLE `supplies` DISABLE KEYS */;
INSERT INTO `supplies` VALUES (1,'2023-12-07',1,'ООО \"Агроном\"','Яблоки Зеленые',1.00,1.00),(2,'2023-12-07',1,'ООО \"Агроном\"','Груши Вильямс',1.00,11.00),(3,'2023-12-07',2,'ИП Яблоков В. А.','Груши Деведжи',1.00,1.00),(4,'2023-12-07',2,'ИП Яблоков В. А.','Груши Вильямс',1.00,1.00),(5,'2023-12-07',1,'ООО \"Агроном\"','Яблоки Зеленые',1.00,1.00),(6,'2023-12-07',1,'ООО \"Агроном\"','Яблоки Красные',1.00,1.00),(7,'2023-12-07',1,'ООО \"Агроном\"','Груши Деведжи',1.00,1.00),(8,'2023-12-07',1,'ООО \"Агроном\"','Яблоки Красные',1.00,1.00),(9,'2023-12-07',3,'ООО \"Центр фруктов\"','Яблоки Зеленые',1.00,1.00),(10,'2023-12-07',3,'ООО \"Центр фруктов\"','Яблоки Красные',1.00,1.00),(11,'2023-12-07',1,'ООО \"Агроном\"','Яблоки Красные',1.00,1.00),(12,'2023-12-07',2,'ИП Яблоков В. А.','Яблоки Зеленые',1.00,1.00);
/*!40000 ALTER TABLE `supplies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-07 22:37:03
